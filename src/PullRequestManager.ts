import * as Sentry from '@sentry/node';
import {
  ErrorCode,
  WebAPICallError,
  WebAPICallResult,
  WebClient
} from '@slack/client';
import DbManager from './database';
import { BitBucket, PullRequestMessageInfo } from './definitions';
import { extractBitbucketPRLinks, isSlackWebApiPlatformError } from './utils';

enum ReactionTo {
  ROBOT = 'robot_face',
  PR_APPROVAL = 'thumbsup',
  PR_DECLINED = 'heavy_multiplication_x',
  PR_COMMENT = 'speech_balloon',
  PR_MERGED = 'heavy_check_mark',
  DISAPPOINTED = 'disappointed',
  DEFAULT = 'negative_squared_cross_mark'
}

const getReactionForEventKey = (eventKey: BitBucket.EVENT_KEY): string => {
  switch (eventKey) {
    case BitBucket.PULL_REQUEST_EVENT_KEY.UNAPPROVED:
    case BitBucket.PULL_REQUEST_EVENT_KEY.APPROVED:
      return ReactionTo.PR_APPROVAL;
    case BitBucket.PULL_REQUEST_EVENT_KEY.MERGED:
      return ReactionTo.PR_MERGED;
    case BitBucket.PULL_REQUEST_EVENT_KEY.DECLINED:
      return ReactionTo.PR_DECLINED;
    case BitBucket.PULL_REQUEST_EVENT_KEY.COMMENT_CREATED:
    case BitBucket.PULL_REQUEST_EVENT_KEY.COMMENT_UPDATED:
    case BitBucket.PULL_REQUEST_EVENT_KEY.COMMENT_DELETED:
      return ReactionTo.PR_COMMENT;
    default:
      return ReactionTo.DEFAULT;
  }
};

const slackErrorsToIgnore = [
  'message_not_found', //
  'already_reacted'
];

class PullRequestManager {
  private readonly _slackClient: WebClient = new WebClient(process.env.SLACK_BOT_USER_TOKEN);

  // region _reactToMessage(s)
  /**
   * Add a given reaction to the given message.
   *
   * If the reaction already exists, and an `'already_reacted'` platform
   * error is thrown by the WebClient, the error is resolved, rather than thrown.
   *
   * @param {string} reactionName
   * @param {string} timestamp
   * @param {string} channel
   *
   * @return {Promise<WebAPICallResult>}
   * @private
   *
   * @throws {WebAPICallError}
   */
  private async _reactToMessage(
    reactionName: string,
    { timestamp, channel }: { timestamp: string; channel: string }
  ): Promise<WebAPICallResult> {
    try {
      return await this._slackClient.reactions.add({
        name: reactionName,
        timestamp,
        channel
      });
    } catch (error) {
      if (
        isSlackWebApiPlatformError(error) &&
        slackErrorsToIgnore.includes(error.data.error)
      ) {
        Sentry.addBreadcrumb({
          message: `${this._reactToMessage.name}: ignoring ${error.data.error} error`,
          data: {
            reaction: reactionName,
            timestamp,
            channel,
            error: error.data
          }
        });

        return error.data;
      }

      throw error;
    }
  }

  /**
   * Add a given reaction to all the given messages.
   *
   * @param reactionName
   * @param messages
   *
   * @return {Promise<Array<WebAPICallResult>>}
   * @private
   */
  private _reactToMessages(reactionName: string, messages: Array<{ timestamp: string, channel: string }>): Promise<Array<WebAPICallResult>> {
    return Promise.all(messages.map(value => this._reactToMessage(reactionName, value)));
  }

  // endregion
  // region _unreactToMessage(s)
  /**
   * Removes a given reaction from the given message
   *
   * If the reaction doesn't exist on the message, and an `'no_reaction'` platform
   * error is thrown by the WebClient, the error is resolved, rather than thrown.
   *
   * @param {string} reactionName
   * @param {string} timestamp
   * @param {string} channel
   *
   * @return {Promise<WebAPICallResult>}
   * @private
   *
   * @throws {WebAPICallError}
   */
  private _unreactToMessage(reactionName: string, { timestamp, channel }: { timestamp: string, channel: string }): Promise<WebAPICallResult> {
    return this._slackClient.reactions.remove({
      name: reactionName,
      timestamp,
      channel
    }).catch((error: WebAPICallError) => {
      if (error.code === ErrorCode.PlatformError && error.data.error === 'no_reaction') {
        Sentry.addBreadcrumb({
          message: `${this._unreactToMessage.name}: ignoring ${error.data.error} error`,
          data: {
            reaction: reactionName,
            timestamp,
            channel,
            error: error.data
          }
        });

        return error.data;
      } // if the reaction isn't there, just act like it was successful

      throw error;
    });
  }

  /**
   * Removes a given reaction from all the given messages.
   *
   * @param reactionName
   * @param messages
   *
   * @return {Promise<Array<WebAPICallResult>>}
   * @private
   */
  private _unreactToMessages(reactionName: string, messages: Array<{ timestamp: string, channel: string }>): Promise<Array<WebAPICallResult>> {
    return Promise.all(messages.map(value => this._unreactToMessage(reactionName, value)));
  }

  // endregion

  public async updateReactionsForPullRequest(
    approved: boolean,
    commentCreated: boolean,
    state: BitBucket.PULL_REQUEST_STATE,
    ownerName: string,
    repoName: string,
    prNumber: string
  ) {
    const affectedMessages = DbManager.prsCollection.find({
      ownerName,
      repoName,
      prNumber
    });

    Sentry.addBreadcrumb({
      message: `${this.updateReactionsForPullRequest.name}: reacting to ${affectedMessages.length} messages`,
      data: { messages: affectedMessages }
    });

    if (affectedMessages.length === 0) {
      console.log(
        `no messages contain the pr ${ownerName}/${repoName}#${prNumber}`
      );

      return;
    }

    if (commentCreated) {
      await this._reactToMessages(
        getReactionForEventKey(
          BitBucket.PULL_REQUEST_EVENT_KEY.COMMENT_CREATED
        ),
        affectedMessages
      );
    }

    const approveReaction = getReactionForEventKey(
      BitBucket.PULL_REQUEST_EVENT_KEY.APPROVED
    );

    await (approved ? this._reactToMessages : this._unreactToMessages).call(
      this,
      approveReaction,
      affectedMessages
    );

    if (state === BitBucket.PULL_REQUEST_STATE.DECLINED) {
      await this._reactToMessages(
        getReactionForEventKey(BitBucket.PULL_REQUEST_EVENT_KEY.DECLINED),
        affectedMessages
      );
    }

    if (state === BitBucket.PULL_REQUEST_STATE.MERGED) {
      await this._reactToMessages(
        getReactionForEventKey(BitBucket.PULL_REQUEST_EVENT_KEY.MERGED),
        affectedMessages
      );
    }
  }

  /**
   * Checks slack messages to see if they've got any `BitBucket` style `PR` links.
   *
   * @param {string} text
   * @param {string} channel
   * @param {string} timestamp
   * @param {boolean} [unreact=false]
   *
   * @return {Promise<void>}
   */
  public async checkMessageForPullRequestLinks(text: string, channel: string, timestamp: string, unreact: boolean = false): Promise<void> {
    const prLinks = extractBitbucketPRLinks(text);

    if (prLinks.length === 0) {
      if (unreact) {
        const messages = [{ timestamp, channel }];

        await Promise.all(Object.values(ReactionTo).map(reaction => this._unreactToMessages(reaction, messages)));
      }

      return;
    } // this message doesn't have a pr link in it

    if (prLinks.length > 1) {
      console.warn('no support for multiple pr links in a single message');

      await this._reactToMessage(ReactionTo.ROBOT, { channel, timestamp });
      await this._reactToMessage(ReactionTo.DISAPPOINTED, { channel, timestamp });

      return;
    }

    const prMessages: Array<PullRequestMessageInfo> = prLinks.map(prInfo => ({
      timestamp,
      channel,
      ...prInfo
    }));

    const newPRs = prMessages.filter(prMessage => !DbManager.prsCollection.findOne(prMessage));

    if (newPRs.length !== prMessages.length) {
      console.warn(`ignoring ${prMessages.length - newPRs.length} messages as they already exist in the db`);
    }

    DbManager.prsCollection.insert(newPRs);

    // mark the message as having been seen & processed
    await this._reactToMessages(ReactionTo.ROBOT, newPRs);
  }
}

export default PullRequestManager;
