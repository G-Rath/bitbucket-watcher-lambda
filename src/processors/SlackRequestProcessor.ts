import * as HttpStatus from 'http-status-codes';
import { ApiGatewayRequest, ApiGatewayResponse, Slack } from '../definitions';
import PullRequestManager from '../PullRequestManager';
import AbstractRequestProcessor from './AbstractRequestProcessor';
import DbManager from './../database';

const { verifyRequestSignature } = require('@slack/events-api');

class SlackRequestProcessor extends AbstractRequestProcessor {
  private _verifySlackRequest(request: ApiGatewayRequest) {
    return verifyRequestSignature({
      signingSecret: process.env.SLACK_SIGNING_SECRET,
      requestSignature: request.headers['X-Slack-Signature'],
      requestTimestamp: request.headers['X-Slack-Request-Timestamp'],
      body: request.body || ''
    });
  }

  /**
   *
   * @param {ApiGatewayRequest} request
   *
   * @return {Object}
   * @private
   */
  private _constructSlackMessageBody(request: ApiGatewayRequest): Slack.MessageEvent {
    const requestBody = JSON.parse(request.body);

    if (requestBody.payload && request.headers['Content-Type'] === 'application/x-www-form-urlencoded') {
      return JSON.parse(requestBody.payload);
    } // interactive messages come in a weird format

    return requestBody.event;
  };

  /**
   *
   * @param {string} message
   *
   * @return {?string}
   * @private
   *
   * @instance
   *
   * @throws {Error}
   */
  private _getMessageText(message: Slack.MessageEvent): string | null {
    if ('subtype' in message) {
      if (Object.values(Slack.MessageSubtype).includes(message.subtype)) {
        return message.subtype === Slack.MessageSubtype.MESSAGE_CHANGED ? message.message.text : null;
      }

      throw new Error(`undefined message subtype: ${message.subtype}`);
    }

    return message.text;
  }

  public async process(request: ApiGatewayRequest): Promise<ApiGatewayResponse | null> {
    if (!request.headers['X-Slack-Signature']) {
      return null;
    } // ignore requests that are not from slack

    console.log('this is a slack message');

    this._verifySlackRequest(request);

    const retriesCount = request.headers['X-Slack-Retry-Num'];

    if (retriesCount) {
      console.warn(`slack has retried this message ${retriesCount} times, b/c ${request.headers['X-Slack-Retry-Reason']}`);
    }

    const requestBody = JSON.parse(request.body);

    if (requestBody.type === 'url_verification') {
      return {
        statusCode: HttpStatus.OK,
        body: requestBody.challenge
      };
    } // handle url_verification separately

    const event = this._constructSlackMessageBody(request);
    let unreact = false;
    let ts = event.ts;

    if (
      'subtype' in event &&
      event.subtype === Slack.MessageSubtype.MESSAGE_CHANGED &&
      event.previous_message
    ) {
      const { previous_message } = event;

      unreact =
        DbManager.prsCollection.find({ timestamp: previous_message.ts })
          .length !== 0;

      DbManager.prsCollection.findAndRemove({
        timestamp: event.previous_message.ts
      });

      ts = event.previous_message.ts;
    }

    const text = this._getMessageText(event);

    if (text) {
      await (new PullRequestManager()).checkMessageForPullRequestLinks(text, event.channel, ts, unreact);
    } // ignore empty text & messages that have no text

    return {
      statusCode: HttpStatus.OK,
      body: {}
    };
  }
}

export default SlackRequestProcessor;
