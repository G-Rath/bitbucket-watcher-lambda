import * as HttpStatus from 'http-status-codes';
import { ApiGatewayRequest, ApiGatewayResponse } from '../definitions';
import AbstractRequestProcessor from './AbstractRequestProcessor';

class HelloWorldRequestProcessor extends AbstractRequestProcessor {
  public async process(request: ApiGatewayRequest): Promise<ApiGatewayResponse | null> {
    return {
      statusCode: HttpStatus.OK,
      body: { message: 'Hello World!' }
    };
  }
}

export default HelloWorldRequestProcessor;
