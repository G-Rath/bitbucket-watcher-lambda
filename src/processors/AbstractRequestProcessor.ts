import { ApiGatewayRequest, ApiGatewayResponse } from '../definitions';

export interface RequestProcessor {
  process(request: ApiGatewayRequest): Promise<ApiGatewayResponse | null>;

  /**
   * Checks if this `RequestProcessor` can process the given `ApiGatewayRequest`.
   *
   * @param {ApiGatewayRequest} request
   *
   * @return {boolean}
   */
  canProcessRequest?(request: ApiGatewayRequest): boolean;
}

abstract class AbstractRequestProcessor implements RequestProcessor {
  abstract async process(request: ApiGatewayRequest): Promise<ApiGatewayResponse | null>;
}

export default AbstractRequestProcessor;
