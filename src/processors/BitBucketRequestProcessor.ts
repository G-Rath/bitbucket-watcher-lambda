import * as HttpStatus from 'http-status-codes';
import { ApiGatewayRequest, ApiGatewayResponse, BitBucket } from '../definitions';
import PullRequestManager from '../PullRequestManager';
import AbstractRequestProcessor from './AbstractRequestProcessor';

class BitBucketRequestProcessor extends AbstractRequestProcessor {
  // region getters & setters
  get prManager(): PullRequestManager {
    return new PullRequestManager();
  }

  // endregion

  private _isPullRequestApproved(pullRequest: BitBucket.Entities.PullRequest) {
    return pullRequest.participants.reduce((isApproved, participant) => isApproved || participant.approved, false);
  }

  private async _processPullRequestEventKey(
    eventKey: BitBucket.PULL_REQUEST_EVENT_KEY,
    payload: BitBucket.Event.PullRequest.Approved
  ) {
    const {
      repository: { full_name: fullName },
      pullrequest: { id: prNumber }
    } = payload;

    const [ownerName, repoName] = fullName.split('/');

    await this.prManager.updateReactionsForPullRequest(
      this._isPullRequestApproved(payload.pullrequest),
      eventKey === BitBucket.PULL_REQUEST_EVENT_KEY.COMMENT_CREATED,
      payload.pullrequest.state,
      ownerName,
      repoName,
      String(prNumber)
    );
  }

  public async process(request: ApiGatewayRequest): Promise<ApiGatewayResponse | null> {
    if (!request.headers['X-Event-Key']) {
      return null;
    } // ignore requests that are not from bitbucket

    console.log('this is a bitbucket message');

    const eventKey = request.headers['X-Event-Key'] as BitBucket.EVENT_KEY;
    const payload = JSON.parse(request.body);

    if (!payload) {
      console.warn('no payload!');
      // todo: actually do something (error handling)
    }

    switch (eventKey) {
      case BitBucket.PULL_REQUEST_EVENT_KEY.APPROVED:
      case BitBucket.PULL_REQUEST_EVENT_KEY.MERGED:
      case BitBucket.PULL_REQUEST_EVENT_KEY.UNAPPROVED:
      case BitBucket.PULL_REQUEST_EVENT_KEY.COMMENT_CREATED:
        await this._processPullRequestEventKey(eventKey, payload);
        break;
    }

    return {
      statusCode: HttpStatus.OK,
      body: { message: 'Hello World!' }
    };
  }
}

export default BitBucketRequestProcessor;
