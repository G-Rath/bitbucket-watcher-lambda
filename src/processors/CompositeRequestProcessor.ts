import * as HttpStatus from 'http-status-codes';
import { ApiGatewayRequest, ApiGatewayResponse } from '../definitions';
import { RequestProcessor } from './AbstractRequestProcessor';

class CompositeRequestProcessor implements RequestProcessor {
  private readonly _processors: Array<RequestProcessor>;

  private readonly _canProcessRequestMethod: (request: ApiGatewayRequest) => boolean = () => true;

  /**
   *
   * @param {Array<RequestProcessor>} processors
   * @param canProcessRequest
   */
  constructor(processors: Array<RequestProcessor>, canProcessRequest?: (request: ApiGatewayRequest) => boolean) {
    this._processors = [...processors];

    if (typeof canProcessRequest === 'function') {
      this.canProcessRequest = canProcessRequest;
    }
  }

  public canProcessRequest(request: ApiGatewayRequest): boolean {
    return this._canProcessRequestMethod(request);
  }

  public async process(request: ApiGatewayRequest): Promise<ApiGatewayResponse> {
    for (const processor of this._processors) {
      if (processor.canProcessRequest && !processor.canProcessRequest(request)) {
        continue;
      }

      const response = await processor.process(request);

      if (response !== null) {
        return response;
      }
    }

    return {
      statusCode: HttpStatus.NOT_IMPLEMENTED,
      body: { message: 'no processor has been registered that can handle this request' }
    };
  }
}

export default CompositeRequestProcessor;
