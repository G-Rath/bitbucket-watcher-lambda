import * as AWS from 'aws-sdk';
import * as LokiJS from 'lokijs';
import LokiS3FileAdapter from './LokiS3FileAdapter';

const projectPackage = require('./../package.json');

export interface S3DatabaseOptions {
  dbName?: string;
  bucketName: string;
}

class S3Database {
  private readonly _db: Loki;

  /**
   *
   * @param {string} dbName
   * @param {string} bucketName
   */
  constructor({ dbName = `${projectPackage.name}.db.json`, bucketName }: S3DatabaseOptions) {
    /**
     *
     * @type {Loki}
     * @private
     */
    this._db = new LokiJS(dbName, {
      autosave: false,
      adapter: new LokiS3FileAdapter({
        s3: new AWS.S3(),
        bucketName,
        formatOnSave: true
      })
    });
  }

  // region getters & setters
  /**
   *
   * @return {Loki}
   */
  get db() {
    return this._db;
  }

  // endregion
  /**
   * Save the database to `S3`.
   *
   * @return {Promise<this>}
   */
  save(): Promise<this> {
    return new Promise((resolve, reject) => this._db.saveDatabase(err => (err ? reject : resolve)(err || this)));
  }

  /**
   * Load the database from `S3`.
   *
   * @return {Promise<this>}
   */
  load(): Promise<this> {
    return new Promise((resolve, reject) => this._db.loadDatabase({}, err => (err ? reject : resolve)(err || this)));
  }
}

export default S3Database;
