import * as HttpStatus from 'http-status-codes';
import { db, dbLoaded, dbSaved } from './database';
import { ApiGatewayRequest, ApiGatewayResponse } from './definitions';
import BitBucketRequestProcessor from './processors/BitBucketRequestProcessor';
import LambdaRequestProcessor from './processors/CompositeRequestProcessor';
import HelloWorldRequestProcessor from './processors/HelloWorldRequestProcessor';
import SlackRequestProcessor from './processors/SlackRequestProcessor';
import * as Sentry from '@sentry/node';

Sentry.init({ dsn: process.env.SENTRY_DNS });

const handleApiGatewayRequestEvent = async (event: ApiGatewayRequest, db: Loki): Promise<ApiGatewayResponse> => {
  try {
    return await (new LambdaRequestProcessor([
      new SlackRequestProcessor(),
      new BitBucketRequestProcessor(),
      new HelloWorldRequestProcessor()
    ])).process(event);
  } catch (error) {
    Sentry.captureException(error);
    console.error(error);

    return {
      statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
      body: error
    };
  }
};

export const handler = async (event: ApiGatewayRequest): Promise<ApiGatewayResponse> => {
  Sentry.addBreadcrumb({
    message: 'received event',
    level: Sentry.Severity.Log,
    data: { ...event }
  });

  console.log('Received event:', JSON.stringify(event, null, 2));

  await dbLoaded();
  const response = await handleApiGatewayRequestEvent(event, db);
  await dbSaved();

  return response;
};
