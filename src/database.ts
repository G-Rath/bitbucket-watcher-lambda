import { PullRequestMessageInfo } from './definitions';
import S3Database from './S3Database';

const bucket = 'auto-storage';

export const s3Database = new S3Database({ bucketName: `${bucket}/tad` });

export const db = s3Database.db;

export const dbLoaded = () => s3Database.load();
export const dbSaved = () => s3Database.save();

export default class DbManager {
  /**
   *
   * @return {Collection<PullRequestMessageInfo>}
   */
  public static get prsCollection(): Collection<PullRequestMessageInfo> {
    return db.addCollection<PullRequestMessageInfo>('prs', {
      indices: ['repoName', 'prNumber'],
      disableMeta: true
    });
  }
}
