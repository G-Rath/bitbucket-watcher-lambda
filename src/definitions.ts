export interface ApiGatewayRequestContext {
  resourceId: string;
  resourcePath: string;
  httpMethod: string;
  extendedRequestId: string;
  requestTime: string;
  path: string;
  accountId: string;
  protocol: string;
  stage?: string;
  domainPrefix: string;
  requestTimeEpoch: number;
  requestId: string;
  identity?: unknown;
  domainName: string;
  apiId: string;
}

export interface ApiGatewayRequest {
  resource: string;
  path: string;
  httpMethod: string;
  headers: Record<string, string | undefined>;
  multiValueHeaders: Record<string, string[]>;
  multiValueQueryStringParameters: Record<string, string[]> | null;
  queryStringParameters: Record<string, string | undefined> | null;
  pathParameters: Record<string, string | undefined> | null;
  stageVariables: Record<string, string | undefined> | null;
  requestContext: ApiGatewayRequestContext;
  body: string;
  isBase64Encoded: boolean;
}

export interface ApiGatewayResponse {
  statusCode: number;
  body: string | object;
}

export interface PullRequestInfo {
  fullLink: string;
  ownerName: string;
  repoName: string;
  prNumber: string;
}

export interface PullRequestMessageInfo extends PullRequestInfo {
  fullLink: string;
  ownerName: string;
  repoName: string;
  prNumber: string;
  timestamp: string;
  channel: string;
}

export namespace Slack {
  export enum MessageSubtype {
    /**
     * A message was posted by an integration
     */
    BOT_MESSAGE = 'bot_message',
    BOT_ADD = 'bot_add',
    /**
     * A channel was archived
     * @type {string}
     */
    CHANNEL_ARCHIVE = 'channel_archive',
    /**
     * A member joined a channel
     * @type {string}
     */
    CHANNEL_JOIN = 'channel_join',
    /**
     * A member left a channel
     * @type {string}
     */
    CHANNEL_LEAVE = 'channel_leave',
    /**
     * A channel was renamed
     * @type {string}
     */
    CHANNEL_NAME = 'channel_name',
    /**
     * A channel purpose was updated
     * @type {string}
     */
    CHANNEL_PURPOSE = 'channel_purpose',
    /**
     * A channel topic was updated
     * @type {string}
     */
    CHANNEL_TOPIC = 'channel_topic',
    /**
     * A channel was unarchived
     * @type {string}
     */
    CHANNEL_UNARCHIVE = 'channel_unarchive',
    /**
     * Message content redacted due to Enterprise Key Management (EKM)
     * @type {string}
     */
    EKM_ACCESS_DENIED = 'ekm_access_denied',
    /**
     * A comment was added to a file
     * @type {string}
     */
    FILE_COMMENT = 'file_comment',
    /**
     * A file was mentioned in a channel
     * @type {string}
     */
    FILE_MENTION = 'file_mention',
    /**
     * A file was shared into a channel
     * @type {string}
     */
    FILE_SHARE = 'file_share',
    /**
     * A group was archived
     * @type {string}
     */
    GROUP_ARCHIVE = 'group_archive',
    /**
     * A member joined a group
     * @type {string}
     */
    GROUP_JOIN = 'group_join',
    /**
     * A member left a group
     * @type {string}
     */
    GROUP_LEAVE = 'group_leave',
    /**
     * A group was renamed
     * @type {string}
     */
    GROUP_NAME = 'group_name',
    /**
     * A group purpose was updated
     * @type {string}
     */
    GROUP_PURPOSE = 'group_purpose',
    /**
     * A group topic was updated
     * @type {string}
     */
    GROUP_TOPIC = 'group_topic',
    /**
     * A group was unarchived
     * @type {string}
     */
    GROUP_UNARCHIVE = 'group_unarchive',
    /**
     * A /me message was sent
     * @type {string}
     */
    ME_MESSAGE = 'me_message',
    /**
     * A message was changed
     * @type {string}
     */
    MESSAGE_CHANGED = 'message_changed',
    /**
     * A message was deleted
     * @type {string}
     */
    MESSAGE_DELETED = 'message_deleted',
    /**
     * A message thread received a reply
     * @type {string}
     */
    MESSAGE_REPLIED = 'message_replied',
    /**
     * An item was pinned in a channel
     * @type {string}
     */
    PINNED_ITEM = 'pinned_item',
    /**
     * (No longer served) A message thread's reply was broadcast to a channel
     * @type {string}
     */
    REPLY_BROADCAST = 'reply_broadcast',
    /**
     * A message thread's reply was broadcast to a channel
     * @type {string}
     */
    THREAD_BROADCAST = 'thread_broadcast',
    /**
     * An item was unpinned from a channel
     * @type {string}
     */
    UNPINNED_ITEM = 'unpinned_item',
    REMINDER_ADD = 'reminder_add',
    APP_CONVERSATION_JOIN = 'app_conversation_join',
    SH_ROOM_CREATED = 'sh_room_created'
  }

  export interface MessageChangeEvent {
    hidden: boolean;
    message: Message;
    previous_message?: EditedMessage;
    subtype: MessageSubtype.MESSAGE_CHANGED;
    ts: string;
    type: string;
    user: string;
  }

  export interface BotMessageEvent {
    subtype: MessageSubtype.BOT_MESSAGE;
    bot_id: string;
    username: string;
    ts: string;
    text: string;
  }

  export interface EditedMessage extends Message {
    edited?: {
      user: string;
      ts: string;
    }
  }

  export interface EventMessageTop {
    channel: string;
    channel_type: string;
    event_ts: string;
  }

  export type MessageEvent = EventMessageTop & (Message | MessageChangeEvent | BotMessageEvent);

  export interface Message {
    client_msg_id: string;
    text: string;
    type: string;
    user: string;
    ts: string;
  }
}

export namespace BitBucket {
  export type EVENT_KEY = PULL_REQUEST_EVENT_KEY;

  export enum PULL_REQUEST_EVENT_KEY {
    CREATED = 'pullrequest:created',
    UPDATED = 'pullrequest:updated',
    APPROVED = 'pullrequest:approved',
    UNAPPROVED = 'pullrequest:unapproved',
    MERGED = 'pullrequest:fulfilled',
    DECLINED = 'pullrequest:rejected',
    COMMENT_CREATED = 'pullrequest:comment_created',
    COMMENT_UPDATED = 'pullrequest:comment_updated',
    COMMENT_DELETED = 'pullrequest:comment_deleted'
  }

  export enum PULL_REQUEST_STATE {
    OPEN = 'OPEN',
    MERGED = 'MERGED',
    DECLINED = 'DECLINED'
  }

  // region namespace: Entities
  export namespace Entities {
    export type AccountType =
      | 'user'
      | 'team'
      ;

    export type EntityType =
      | 'project'
      | 'repository'
      | AccountType
      ;

    export interface Link {
      href: string;
    }

    export interface MapOfLinks {
      [k: string]: Link
    }

    export interface Entity {
      type: EntityType;
      uuid: string;
      links: MapOfLinks;
    }

    export interface Owner extends Entity {
      type: AccountType;
      username: string;
      display_name: string;
      uuid: string;
    }

    export interface User extends Owner {
      type: 'user';
      username: string;
      display_name: string;
      uuid: string;
    }

    // region namespace: Issue
    export type Issue = Issue.Issue;

    export namespace Issue {
      export enum State {
        NEW = 'new',
        OPEN = 'open',
        ON_HOLD = 'on hold', // todo: does it really have a space in it?
        RESOLVED = 'resolved',
        DUPLICATE = 'duplicate',
        INVALID = 'invalid',
        WONT_FIX = 'wontfix',
        CLOSED = 'closed'
      }

      export enum Type {
        BUG = 'bug',
        ENHANCEMENT = 'enhancement',
        PROPOSAL = 'proposal',
        TASK = 'task'
      }

      export enum Priority {
        TRIVIAL = 'trivial',
        MINOR = 'minor',
        MAJOR = 'major',
        CRITICAL = 'critical',
        BLOCKER = 'blocker'
      }

      export interface Issue {
        id: number;
        component: string;
        title: string;
        content: {
          raw: string;
          html: string;
          markup: string;
        };
        priority: Priority;
        state: State;
        type: Type;
        milestone: { name: string };
        version: { name: string };
        created_on: string;
        updated_on: string;
      }
    }

    // endregion

    export interface Comment {
      /**
       * The id number of the comment.
       */
      id: number;
      /**
       * The id number of the comment's parent comment.
       */
      parent: { id: number };
      /**
       * The actual comment.
       */
      content: {
        raw: string;
        html: string;
        markup: string;
      };
      /**
       * An indication of whether the comment is an inline code comment.
       */
      inline: {
        /**
         * The name of the file the comment was made on.
         */
        path: string;
        /**
         * The line number of the old version in the diff that the comment was made on.
         * (the "red" line).
         *
         * `null` if the line does not exist in the old version.
         */
        from: number | null;
        /**
         * The line number of the new version in the diff that the comment was made on.
         * (the "green" line).
         *
         * `null` if the line does not exist in the new version.
         */
        to: number | null;
      };
      /**
       * The date and time (in ISO 8601 format) the issue was created.
       */
      created_on: string;
      /**
       * The date and time (in ISO 8601 format) the issue was last updated.
       */
      updated_on: string;
    }

    export interface Repository extends Entity {
      type: 'repository';
      name: string;
      /**
       * The username or team name where the repository is located,
       * and the name of the user.
       *
       * @example `${team_name}/${repo_name}`
       */
      full_name: string;
      project: Project;
      website: string;
      owner: Owner;
      scm: 'git' | 'hg';
      is_private: boolean;
    }

    export interface Project extends Entity {
      type: 'project';
      name: string;
      uuid: string;
      key: string;
    }

    export interface Participant {
      type: 'participant';
      role: 'PARTICIPANT';
      user: Owner;
      approved: boolean;
      participated_on: string;
    }

    export interface PullRequest extends Entity {
      id: number;
      title: string;
      description: string;
      state: PULL_REQUEST_STATE;
      author: Owner;
      source: {
        branch: {
          name: string;
        }
        commit: {
          hash: string;
        }
        repository: Entities.Repository;
      }
      destination: {
        branch: {
          name: string;
        }
        commit: {
          hash: string;
        }
        repository: Entities.Repository;
      }
      merge_commit: {
        hash: string;
      }
      participants: Array<Participant>;
      reviewers: Array<Owner>;
      close_source_branch: boolean;
      closed_by: Owner;
      reason: string;
      created_on: string;
      updated_on: string;
    }
  }
  // endregion
  // region namespace: Event
  export namespace Event {
    export interface EventPayload {

    }

    // region namespace: PullRequest
    export namespace PullRequest {
      // todo create event-key header map

      export interface PullRequestEventPayload extends EventPayload {
        actor: Entities.Owner;
        pullrequest: Entities.PullRequest;
        repository: Entities.Repository;
      }

      export interface Created extends PullRequestEventPayload {

      }

      export interface Updated extends PullRequestEventPayload {

      }

      export interface Approved extends PullRequestEventPayload {
        approval: {
          date: string;
          user: Entities.Owner;
        }
      }

      export interface Unapproved extends PullRequestEventPayload {
        approval: {
          date: string;
          user: Entities.Owner;
        }
      }

      export interface Merged extends PullRequestEventPayload {

      }

      export interface Declined extends PullRequestEventPayload {

      }

      export interface CommentCreated extends PullRequestEventPayload {
        comment: Entities.Comment;
      }

      export interface CommentUpdated extends PullRequestEventPayload {
        comment: Entities.Comment;
      }

      export interface CommentDeleted extends PullRequestEventPayload {
        comment: Entities.Comment;
      }
    }
    // endregion
  }
  // endregion
}
// X-Slack-Signature = slack
// X-Event-Key = bitbucket (+ event key trigger)

// @ts-ignore
const dump = {
  'path': '/test/hello',
  'headers': {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate, lzma, sdch, br',
    'Accept-Language': 'en-US,en;q=0.8',
    'CloudFront-Forwarded-Proto': 'https',
    'CloudFront-Is-Desktop-Viewer': 'true',
    'CloudFront-Is-Mobile-Viewer': 'false',
    'CloudFront-Is-SmartTV-Viewer': 'false',
    'CloudFront-Is-Tablet-Viewer': 'false',
    'CloudFront-Viewer-Country': 'US',
    'Host': 'wt6mne2s9k.execute-api.us-west-2.amazonaws.com',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.82 Safari/537.36 OPR/39.0.2256.48',
    'Via': '1.1 fb7cca60f0ecd82ce07790c9c5eef16c.cloudfront.net (CloudFront)',
    'X-Amz-Cf-Id': 'nBsWBOrSHMgnaROZJK1wGCZ9PcRcSpq_oSXZNQwQ10OTZL4cimZo3g==',
    'X-Forwarded-For': '192.168.100.1, 192.168.1.1',
    'X-Forwarded-Port': '443',
    'X-Forwarded-Proto': 'https'
  },
  'pathParameters': {
    'proxy': 'hello'
  },
  'requestContext': {
    'accountId': '123456789012',
    'resourceId': 'us4z18',
    'stage': 'test',
    'requestId': '41b45ea3-70b5-11e6-b7bd-69b5aaebc7d9',
    'identity': {
      'cognitoIdentityPoolId': '',
      'accountId': '',
      'cognitoIdentityId': '',
      'caller': '',
      'apiKey': '',
      'sourceIp': '192.168.100.1',
      'cognitoAuthenticationType': '',
      'cognitoAuthenticationProvider': '',
      'userArn': '',
      'userAgent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.82 Safari/537.36 OPR/39.0.2256.48',
      'user': ''
    },
    'resourcePath': '/{proxy+}',
    'httpMethod': 'GET',
    'apiId': 'wt6mne2s9k'
  },
  'resource': '/{proxy+}',
  'httpMethod': 'GET',
  'queryStringParameters': {
    'name': 'me'
  },
  'stageVariables': {
    'stageVarName': 'stageVarValue'
  }
};
