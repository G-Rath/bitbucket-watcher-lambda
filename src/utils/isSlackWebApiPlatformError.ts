import { ErrorCode } from '@slack/client';
import { WebAPICallError, WebAPIPlatformError } from '@slack/web-api';

export const isSlackWebApiPlatformError = (
  error: WebAPICallError
): error is WebAPIPlatformError => error.code === ErrorCode.PlatformError;
