import * as LinkifyIt from 'linkify-it';
import { PullRequestInfo } from '../definitions';

const prRegexp = /https:\/\/bitbucket\.org\/(.*?)\/(.*?)(?:\/pull-requests\/)(\d+)/;

const linkifyIt = new LinkifyIt();

/**
 * Extracts BitBucket PR links from the given `text`.
 *
 * @param {string} text
 *
 * @return {PullRequestInfo[]}
 */
export const extractBitbucketPRLinks = (text: string): PullRequestInfo[] =>
  (linkifyIt.match(text) || [])
    .map(({ url }) => url.match(prRegexp))
    .filter((ml): ml is RegExpMatchArray => !!(ml && ml[3]))
    .map(([fullLink, ownerName, repoName, prNumber]) => ({
      fullLink,
      ownerName,
      repoName,
      prNumber
    }));
