import S3 = require('aws-sdk/clients/s3');

export interface LokiS3FileAdapterOptions {
  s3: S3;
  bucketName: string;
  formatOnSave?: boolean;
  errorIfFileNotFound?: boolean;
}

class LokiS3FileAdapter implements LokiPersistenceAdapter {
  private readonly _options: LokiS3FileAdapterOptions;

  /**
   *
   * @param {LokiS3FileAdapterOptions} options
   */
  constructor(options: LokiS3FileAdapterOptions) {
    this._options = options;
  }

  /**
   *
   * @param {string} dbName
   * @param {function(value: *): void} callback
   * @override
   */
  loadDatabase(dbName: string, callback: (value: any) => void): void {
    const params = {
      Bucket: this._options.bucketName,
      Key: dbName
    };

    this._options.s3.getObject(params, (err, data) => {
      if (err) {
        if (!this._options.errorIfFileNotFound && err.statusCode === 404) {
          return callback({}); // don't panic if file doesn't exist
        } // it'll be made when we save for the first time

        throw new Error('Remote load failed');
      }

      if (!data.Body) {
        throw new Error('Remote load failed, no data found');
      }

      try {
        return callback(JSON.parse(data.Body.toString()));
      } catch (e) {
        throw new Error('Remote load failed, invalid loki database');
      }
    });
  }

  /**
   *
   * @param {string|*} dbNameOrOptions
   * @param {function(err: Error | null, data: *): void} callback
   */
  deleteDatabase(dbNameOrOptions: any, callback: (err?: Error | null, data?: any) => void): void {
    throw Error('deleteDatabase not supported (yet)');
  }

  /**
   *
   * @param {string} dbName
   * @param {Loki} dbRef
   * @param {function(err: Error | null): void} callback
   */
  exportDatabase(dbName: string, dbRef: Loki, callback: (err: Error | null) => void): void {
    throw Error('exportDatabase not supported (yet)');
  }

  /**
   *
   * @param {string} dbName
   * @param {string|*} dbString
   * @param {function(err: Error | null): void} callback
   *
   * @override
   */
  saveDatabase(dbName: string, dbString: any, callback: (err?: Error | null) => void): void {
    const body = this._options.formatOnSave ? JSON.stringify(JSON.parse(dbString), null, 4) : dbString;

    const params = {
      Body: body,
      Bucket: this._options.bucketName,
      Key: dbName,
      ServerSideEncryption: 'AES256',
      Tagging: '' // key1=value1&key2=value2
    };

    this._options.s3.putObject(params, callback);
  }
}

export default LokiS3FileAdapter;
