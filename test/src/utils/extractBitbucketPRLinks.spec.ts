import { EOL } from 'os';
import { PullRequestInfo } from '../../../src/definitions';
import { extractBitbucketPRLinks } from '../../../src/utils';

/**
 * Creates a `PullRequestInfo` object, with the complete `fullLink` that would
 * generated the returned `PullRequestInfo` when extracted & parsed.
 *
 * @param {string} ownerName
 * @param {string} repoName
 * @param {string} prNumber
 *
 * @return {PullRequestInfo}
 */
const buildPRTestCase = (
  ownerName: string,
  repoName: string,
  prNumber: string
): PullRequestInfo => ({
  fullLink: `https://bitbucket.org/${ownerName}/${repoName}/pull-requests/${prNumber}/`,
  ownerName,
  repoName,
  prNumber
});

describe('extractBitbucketPRLinks', () => {
  describe('when the text has links', () => {
    describe('when some of the links are for PRs', () => {
      it('extracts all the PRs', () => {
        const expectedPRs: PullRequestInfo[] = [
          buildPRTestCase('rabidtech', 'sounz', '476'),
          buildPRTestCase('preferizi', 'relm-app', '315')
        ];

        const prs = extractBitbucketPRLinks(
          expectedPRs
            .map(({ fullLink }) => `This is a link to a PR: ${fullLink}`)
            .join(EOL)
        );

        expect(prs).toStrictEqual(expect.arrayContaining(expectedPRs));
        expect(prs).toHaveLength(expectedPRs.length);
      });

      it('extracts only the PRs', () => {
        const expectedPR: PullRequestInfo = buildPRTestCase(
          'rabidtech',
          'sounz',
          '476'
        );

        const prs = extractBitbucketPRLinks(
          [
            `https://www.google.com is not a BitBucket PR link`,
            `But ${expectedPR.fullLink} is.`
          ].join(EOL)
        );

        expect(prs).toHaveLength(1);
        expect(prs[0]).toStrictEqual(expectedPR);
      });

      it('extracts with trailing description', () => {
        const expectedPR: PullRequestInfo = buildPRTestCase(
          'rabidtech',
          'sounz',
          '476'
        );

        const prs = extractBitbucketPRLinks(
          `This link ${expectedPR.fullLink}/my-pull-request is not a BitBucket PR link`
        );

        expect(prs).toHaveLength(1);
        expect(prs[0]).toStrictEqual(expectedPR);
      });
    });

    describe('when none of the links are for PRs', () => {
      it.each([
        'https://github.com/jest-community/eslint-plugin-jest/pull/428',
        'https://bitbucket.org/owner/repo/pull-requests/',
        'file://my-file.json',
        'www.google.com'
      ])('ignores %s', link => {
        const links = extractBitbucketPRLinks(
          `This link ${link} is not a BitBucket PR link`
        );

        expect(links).toHaveLength(0);
      });
    });
  });

  describe('when the text has no links', () => {
    it('extracts no PRs', () => {
      const links = extractBitbucketPRLinks(
        'this is my text! It has no links :('
      );

      expect(links).toHaveLength(0);
    });
  });
});
