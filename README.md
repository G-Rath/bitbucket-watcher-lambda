# @tad/bitbucket-watcher-lambda
This function does things

## Setup
Run `npm install`.

### Setup
* Setup necessary environment variables from `functions.env` directly in AWS Lambda
* Add AWS environment variables for deployment, see [node-lambda](https://www.npmjs.com/package/node-lambda) package for further configuration options:
  * `AWS_ACCESS_KEY_ID`
  * `AWS_SECRET_ACCESS_KEY`
  * `AWS_REGION` (e.g. `ap-southeast-2`)
  * `AWS_DEFAULT_REGION` (e.g. `ap-southeast-2`)
  * `AWS_FUNCTION_NAME` (use function name only, not the ARN)
  * `AWS_ROLE_ARN`
  * `AWS_RUNTIME` (most likely `nodejs8.10`)
  * `EXCLUDE_GLOBS` recommended value: `event.json context.json .idea *.iml README.md *.env *.example test`
  * `PACKAGE_DIRECTORY` recommended value: `build`
