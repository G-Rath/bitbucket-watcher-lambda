/**
 * Ensures that the given `Response` has a body of type `string`.
 *
 * @param {ApiGatewayResponse} response
 *
 * @return {ApiGatewayResponse}
 */
const ensureStringBody = response => ({
  ...response,
  body: typeof response.body === 'object'
    ? JSON.stringify(response.body)
    : response.body
});

// todo: convert this file to ts or something
exports.handler = event => require('./dist/index').handler(event).then(ensureStringBody);
